![](https://i-made.theworstme.me/ea0ad4.png)

[![](https://img.shields.io/discord/307640404071677962.svg)](https://discord.gg/PEW4wx9) [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/772/badge)](https://bestpractices.coreinfrastructure.org/projects/772)
[![](https://img.shields.io/badge/add%20bot-official%20instance-blue.svg)](https://discordapp.com/api/oauth2/authorize?client_id=272549225454239744&scope=bot&permissions=0)

# Warning
This is not meant for your own little cozy Discord bot clone.

**DO NOT** try to submit a clone of this bot to any bot list.

## Requirements

- Python 3 (latest version preferred)
- Some python modules (`pip install -Ur requirements.txt`)
- A text editor that isn't notepad or something
- Knowing how to actually maintain a bot

## Running

1. `git clone https://gitlab.com/lolbot-project/lolbot`
2. `cd lolbot`
3. `nano config.json` (follow the comments beginning with `//`)
4. `python3.6 index.py` 

If you would like to run the bot using systemd, there is a template systemd service file in the `run` directory.

### Credits
- [luna](https://github.com/lnyaa) - Contributions & code
- [slice](https://github.com/slice) - Code
