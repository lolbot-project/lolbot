from cogs.api import app
from discord.ext import commands
from hypercorn import Config
from hypercorn.asyncio import serve


class WebAPILauncher(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.loop = bot.loop
        app.bot = bot
        bot.api_instance = app
        self.config = Config()
        self.config.bind = '127.0.0.1:6142'
        self.start_app()

    def start_app(self):
        self.bot.api_task = self.loop.create_task(serve(app, self.config))

def __unload(bot):
    bot.api_task.cancel()


def setup(bot):
    bot.add_cog(WebAPILauncher(bot))
